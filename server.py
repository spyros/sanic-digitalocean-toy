#!/usr/bin/env python
# -*- coding: utf-8 -*-
import arrow
from sanic import Sanic
from sanic.response import text

app = Sanic("MyHelloWorldApp")

@app.get("/")
async def hello_world(request):
    return text("Ahoy pirate, what are you looking for?")


@app.get("/hi")
async def greet_by_day_phase(request):
    current_hour = arrow.utcnow().hour
    
    if current_hour in (23, 5):
        return text("Late night vibes")

    if current_hour in (6, 9):
        return text("Got up early?")

    if current_hour in (10, 11):
        return text("Ready for 2nd coffee?")

    if current_hour in (12, 16):
        return text("Lunch time")

    if current_hour in (17, 22):
        return text("Relax time")

    return text("What time is it?")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
